<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Vip
 *
 * @ORM\Table(name="vip")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VipRepository")
 * @UniqueEntity("phone",message="Le numéro de téléphone est déja utilisé.")
 */
class Vip
{
    
    public function __construct(){
        $this->activationStatus = true ;
    }
    
    
     /**
     * @Assert\IsTrue(message = "Le format du numéro de téléphone est invalide")
     */
    public function isPhoneValid() 
    {
        return preg_match("/^(?:07|\+?33)(?:\d(?:-)?){9,10}$/m", str_replace(' ', '', $this->getPhone()));
    }
    

    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
      /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;
    
     /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Application
     */

    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }

    /**
     * @var string
     *@Assert\NotBlank(message = "Le nom du destinataire ne doit pas être vide")
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *@Assert\NotBlank(message = "Le numéro de téléphone ne doit pas être vide")
     * @ORM\Column(name="phone", type="string", length=255, unique=true)
     */
    private $phone;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Vip
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Vip
     */
    public function setPhone($phone)
    {
        $this->phone = str_replace(' ', '', $phone);

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
}

