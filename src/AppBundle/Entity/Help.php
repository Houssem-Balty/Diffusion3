<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Help
 *
 * @ORM\Table(name="help")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HelpRepository")
 */
class Help
{



    public function __construct(){
        
    }
    
    
    
 


     /**
     * Set id
     *
     * @param int $id
     *
     * @return Help
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;


    /**
     * @var string
     *@Assert\NotBlank(message = "Le lien ne peut pas être vide.")
     * @ORM\Column(name="link", type="string", length=800)
     */
    private $link;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Help
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
}

