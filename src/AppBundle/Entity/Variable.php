<?php

namespace AppBundle\Entity;
use AppBundle\Entity\Metier;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Template
 *
 * @ORM\Table(name="variable")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VariableRepository")
 */
 class Variable
 {
    
    
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\IsTrue(message = "La variable doit être sous la forme __NomVariable")
     */
    public function isVariableNameValid() 
    {
        return preg_match("/^__[a-zA-Z]{2,10}$/m", str_replace(' ', '', $this->getName()));
    }


    function cleanString($text) {
    $utf8 = array(
        '/[áàâãªä]/u'   =>   'a',
        '/[ÁÀÂÃÄ]/u'    =>   'A',
        '/[ÍÌÎÏ]/u'     =>   'I',
        '/[íìîï]/u'     =>   'i',
        '/[éèêë]/u'     =>   'e',
        '/[ÉÈÊË]/u'     =>   'E',
        '/[óòôõºö]/u'   =>   'o',
        '/[ÓÒÔÕÖ]/u'    =>   'O',
        '/[úùûü]/u'     =>   'u',
        '/[ÚÙÛÜ]/u'     =>   'U',
        '/ç/'           =>   'c',
        '/Ç/'           =>   'C',
        '/ñ/'           =>   'n',
        '/Ñ/'           =>   'N',
        '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
        '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
        '/[“”«»„]/u'    =>   ' ', // Double quote
        '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
    );
    return preg_replace(array_keys($utf8), array_values($utf8), $text);
}
    


    
    public function getId(){
        return $this->id;
    }
    /**
     * @var string
     *@Assert\NotBlank(message = "Le nom de la variable ne doit pas être vide !")
     * @ORM\Column(name="name", type="string")
     */
     private $name;

     public function getName()
     {
         return $this->name;
     }

     public function setName($name)
     {
         $this->name = preg_replace('/[^A-Za-z0-9_\-]/', '',$this->cleanString($name));
     }
 }
