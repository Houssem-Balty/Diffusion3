<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieRepository")
 */
class Categorie
{
    
    public function __contruct(){
        $this->activationStatus  = true ;
    }
    
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner l'intitulé de la catégorie")
     * @ORM\Column(name="nomCategorie", type="string", length=255)
     */
    private $nomCategorie;

    
    
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;
    
    
    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Application
     */

    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCategorie
     *
     * @param string $nomCategorie
     *
     * @return Categorie
     */
    public function setNomCategorie($nomCategorie)
    {
        $this->nomCategorie = $nomCategorie;

        return $this;
    }

    /**
     * Get nomCategorie
     *
     * @return string
     */
    public function getNomCategorie()
    {
        return $this->nomCategorie;
    }
}

