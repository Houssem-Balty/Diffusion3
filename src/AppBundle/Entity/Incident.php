<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Incident
 *
 * @ORM\Table(name="incident")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IncidentRepository")
 */
class Incident
{

    public function __construct(){
      $this->pos = new ArrayCollection();
      $this->sms = new ArrayCollection();
      $this->diffusions = new ArrayCollection();
      $this->impactMetier = new ArrayCollection();
      $this->impactApplication = new ArrayCollection();
      $this->dateDebut = new \DateTime();
      $this->dateFin = new \DateTime();
      $this->dateFinImpact = new \DateTime();
      $this->dateDebutImpact = new \DateTime();
      $this->dateCreation = new \DateTime();

    }

    /**
     * @Assert\IsTrue(message = "La date de début d'incident doit être inférieure à la date de fin")
     */
    public function isDateValid() // verifies que la date de debut d'incident est inférieure à la date de fin d'incident
    {
        if ($this->ddiConnue == 1 && $this->dfiConnue== 1)
        return $this->dateDebut->getTimeStamp() < $this->dateFin->getTimeStamp();
        else
            return true ;
    }
    
    /**
     * @Assert\IsTrue(message = "La date de début d'impact doit être inférieure à la date de fin impact")
     */
    public function isDateImpactValid() // verifies que la date de debut d'incident est inférieure à la date de fin d'incident
    {
        if ($this->ddimConnue == 1 && $this->dfimConnue== 1)
        return $this->dateDebutImpact->getTimeStamp() < $this->dateFinImpact->getTimeStamp();
        else
            return true ;
    }


    public function getLastPosDate(){
        $nbPos = $this->pos->count() - 1 ; 
        if ($this->pos->count() == 0){
            return "Pas de point de situation." ;
        } else if ($this->pos->get($nbPos)->getIsResolution()){
            return "Résolution prévue :" .  $this->pos->get($nbPos)->getResolutionDate()->format('d-m-Y H:i') ;
        } else if (!$this->pos->get($nbPos)->getIsResolution()){
            return $this->pos->get($nbPos)->getNextPosDate()->format('d-m-Y H:i');
        } else {
            return "error" ;
        }
    }

    public function getMetiersImpactes(){
        $resultat = array() ;
        foreach ($this->impactApplication as $application) {
            foreach($application->getMetiers() as $metier){
                if ($metier->getActivationStatus())
                $resultat[] = $metier->getNomMetier() ;    
            }
            
        }

        return array_unique($resultat) ;

    }

    
    /**
     * @Assert\IsTrue(message = "Afin de pouvoir clôturer l'incident veuillez renseigner toutes les dates !")
     */
    public function isNecessaryDatesSpecified() // verifies que la date de debut d'incident est inférieure à la date de fin d'incident
    {
        return !($this->dfiConnue  && (!$this->ddiConnue || !$this->ddimConnue || !$this->dfimConnue) ) ;
    }
    


    /**
     * @Assert\IsTrue(message = "Veuillez renseigner un impact métier ou application.")
     */
    public function isImpactDefined() // verifies que la date de debut d'incident est inférieure à la date de fin d'incident
    {
        return  (!$this->isApplicationImpact && $this->impactApplication->count() > 0) || ($this->isApplicationImpact && $this->impactMetier->count() > 0);
    }
    
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime",nullable=false)
     */
    private $dateCreation;

    

   /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(nullable=false)
    */

    private $emetteur ;
    
    /**
     * Set emetteur
     *
     * @param object $emetteur
     *
     * @return Incident
     */
    public function setEmetteur($emetteur)
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * Get Emetteur
     *
     * @return object
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }
    

    /**
     * One Incident has Many Emails.
     * @ORM\OneToMany(targetEntity="Diffusion", mappedBy="incident", cascade={"persist","remove"})
     */

    private $diffusions ;

    
    
    /**
    *@ORM\OneToMany(targetEntity="Pos", mappedBy="incident", cascade={"persist","remove"})
    *
    */
    private $pos ;
    
    
    
    /**
    *@ORM\OneToMany(targetEntity="Sms", mappedBy="incident", cascade={"persist","remove"})
    *
    */
    private $sms ;

    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner le titre de l'incident.")
     * @ORM\Column(name="Titre", type="string", length=255)
     */
    private $titre;

    
    /**
     *@Assert\NotBlank(message="Veuillez renseigner l'impact")
     * Many Users have One Address.
     * @ORM\ManyToOne(targetEntity="Impact")
     * @ORM\JoinColumn(name="impact", referencedColumnName="id")
     */
    private $impact;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDebut", type="datetime",nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateFin", type="datetime", nullable=true)
     */
    private $dateFin;


    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner le type de l'incident")
     * @ORM\Column(name="Type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner la description de l'incident")
     * @ORM\Column(name="descTechnique", type="string", length=800)
     */
    private $descTechnique;
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=800, nullable=true)
     */
    private $notes;


    /**
     * @var string
     *
     * @ORM\Column(name="rex", type="string", length=800, nullable=true)
     */
    private $rex;


    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(nullable=true)
    */

    private $closer ;
    
    /**
     * Set closer
     *
     * @param object $closer
     *
     * @return Incident
     */
    public function setCloser($closer)
    {
        $this->closer = $closer;

        return $this;
    }

    /**
     * Get closer
     *
     * @return closer
     */
    public function getCloser()
    {
        return $this->closer;
    } 




    /**
     *@Assert\NotBlank(message="Veuillez renseigner le client!")
    * @ORM\ManyToOne(targetEntity="Client")
    * @ORM\JoinColumn(nullable=false)
    */
    private $client;




    /**
     * Many Incidents can have many Impacted Applications
     * @ORM\ManyToMany(targetEntity="Metier")
     * @ORM\JoinTable(name="incident_metier",
     *      joinColumns={@ORM\JoinColumn(name="incident_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="metier_id", referencedColumnName="id")}
     *      )
     */
 
     
    private $impactMetier;


    /**
     * Many Incidents can have many Impacted Applications
     * @ORM\ManyToMany(targetEntity="Application")
     * @ORM\JoinTable(name="incident_application",
     *      joinColumns={@ORM\JoinColumn(name="incident_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id")}
     *      )
     */
    private $impactApplication;



    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=800)
     */

    private $statut ;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="dureeResolution", type="string", length=800, nullable=true)
     */

    private $dureeResolution ;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFinImpact", type="datetime", nullable=true)
     */
    private $dateFinImpact;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebutImpact", type="datetime", nullable=true)
     */
    private $dateDebutImpact;
    
    
   
    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner le ressenti utilisateur")
     * @ORM\Column(name="ressentiUtilisateur", type="string", length=800)
     */
    private $ressentiUtilisateur;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="ddiConnue", type="boolean")
     */
    private $ddiConnue ; // Date de Debut D'incident connue (true ou false)
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="dfiConnue", type="boolean")
     */
    private $dfiConnue ;  // Date de Fin D'incident connue (true ou false)
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="ddimConnue", type="boolean")
     */
    private $ddimConnue ;    // Date de Debut D'impact connue (true ou false)
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="dfimConnue", type="boolean")
     */
    private $dfimConnue ;  // Date de Fin D'impact connue (true ou false)

    
     /**
     * @var boolean
     *
     * @ORM\Column(name="isApplicationImpact", type="boolean")
     */
     
    private $isApplicationImpact ;
    
     /**
     * Set ddiConnue
     *
     * @param boolean $ddiConnue
     *
     * @return Incident
     */
    public function setDdiConnue($ddiConnue)
    {
        $this->ddiConnue = $ddiConnue;

        return $this;
    }

    /**
     * Get ddiConnue
     *
     * @return boolean
     */
    public function getDdiConnue()
    {
        return $this->ddiConnue;
    }
    
    
     /**
     * Set isApplicationImpact
     *
     * @param boolean isApplicationImpact
     *
     * @return Incident
     */
    public function setIsApplicationImpact($isApplicationImpact)
    {
        $this->isApplicationImpact = $isApplicationImpact;

        return $this;
    }

    /**
     * Get isApplicationImpact
     *
     * @return boolean
     */
    public function getIsApplicationImpact()
    {
        return $this->isApplicationImpact;
    }
    
    
    /**
     * Set dfiConnue
     *
     * @param boolean $dfiConnue
     *
     * @return Incident
     */
    public function setDfiConnue($dfiConnue)
    {
        $this->dfiConnue = $dfiConnue;

        return $this;
    }

    /**
     * Get dfiConnue
     *
     * @return boolean
     */
    public function getDfiConnue()
    {
        return $this->dfiConnue;
    }
    
        /**
     * Set dfimConnue
     *
     * @param boolean $dfimConnue
     *
     * @return Incident
     */
    public function setDfimConnue($dfimConnue)
    {
        $this->dfimConnue = $dfimConnue;

        return $this;
    }

    
    /**
     * Get dfimConnue
     *
     * @return boolean
     */
    public function getDfimConnue()
    {
        return $this->dfimConnue;
    }
    
    /**
     * Set ddimConnue
     *
     * @param boolean $ddimConnue
     *
     * @return Incident
     */
    public function setDdimConnue($ddimConnue)
    {
        $this->ddimConnue = $ddimConnue;

        return $this;
    }

    
    /**
     * Get ddimConnue
     *
     * @return boolean
     */
    public function getDdimConnue()
    {
        return $this->ddimConnue;
    }
    
      
    
        
    

    public function addMetierImpacte(Metier $metier){
        $this->impactMetier[] = $metier ;
        return $this ;
    }


     public function addApplicationImpactee(Application $application){
        $this->impactApplication[] = $application ;
        return $this ;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Incident
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }




    /**
     * Get impactMetier
     *
     * @return object
     */
    public function getImpactMetier()
    {
        return $this->impactMetier;
    }


    /**
     * Get impactApplication
     *
     * @return object
     */
    public function getImpactApplication()
    {
        return $this->impactApplication;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    
    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Incident
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }



    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Incident
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set impact
     *
     * @param integer $impact
     *
     * @return Incident
     */
    public function setImpact($impact)
    {
        $this->impact = $impact;

        return $this;
    }

    /**
     * Get impact
     *
     * @return int
     */
    public function getImpact()
    {
        return $this->impact;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Incident
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }


    
    
    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Incident
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Incident
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    
    
     /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Incident
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }
    

    /**
     * Set rex
     *
     * @param string $rex
     *
     * @return Incident
     */
    public function setRex($rex)
    {
        $this->rex = $rex;

        return $this;
    }

    /**
     * Get rex
     *
     * @return string
     */
    public function getRex()
    {
        return $this->rex;
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * Set descTechnique
     *
     * @param string $descTechnique
     *
     * @return Incident
     */
    public function setDescTechnique($descTechnique)
    {
        $this->descTechnique = $descTechnique;

        return $this;
    }

    /**
     * Get descTechnique
     *
     * @return string
     */
    public function getDescTechnique()
    {
        return $this->descTechnique;
    }

    /**
     * Set ressentiUtilisateur
     *
     * @param string $ressentiUtilisateur
     *
     * @return Incident
     */
    public function setRessentiUtilisateur($ressentiUtilisateur)
    {
        $this->ressentiUtilisateur = $ressentiUtilisateur;

        return $this;
    }

    /**
     * Get ressentiUtilisateur
     *
     * @return string
     */
    public function getRessentiUtilisateur()
    {
        return $this->ressentiUtilisateur;
    }

    /**
     * Set actionCorrective
     *
     * @param string $actionCorrective
     *
     * @return Incident
     */
    public function setActionCorrective($actionCorrective)
    {
        $this->actionCorrective = $actionCorrective;

        return $this;
    }

    /**
     * Get actionCorrective
     *
     * @return string
     */
    public function getActionCorrective()
    {
        return $this->actionCorrective;
    }

    /**
     * Set actionPreventive
     *
     * @param string $actionPreventive
     *
     * @return Incident
     */
    public function setActionPreventive($actionPreventive)
    {
        $this->actionPreventive = $actionPreventive;

        return $this;
    }

    /**
     * Get actionPreventive
     *
     * @return string
     */
    public function getActionPreventive()
    {
        return $this->actionPreventive;
    }

    /**
     * Set dateDebutImpact
     *
     * @param \DateTime $dateDebutImpact
     *
     * @return Incident
     */
    public function setDateDebutImpact($dateDebutImpact)
    {
        $this->dateDebutImpact = $dateDebutImpact;

        return $this;
    }

    /**
     * Get dateDebutImpact
     *
     * @return \DateTime
     */
    public function getDateDebutImpact()
    {
        return $this->dateDebutImpact;
    }

    /**
     * Set dateFinImpact
     *
     * @param \DateTime $dateFinImpact
     *
     * @return Incident
     */
    public function setDateFinImpact($dateFinImpact)
    {
        $this->dateFinImpact = $dateFinImpact;

        return $this;
    }
    
    
    /**
     * Set impactMetier
     *
     *
     * @return Incident
     */
    public function setImpactMetier($impactMetier)
    {
        $this->impactMetier = $impactMetier;

        return $this;
    }
    
       
    /**
     * Set impactMetier
     *
     *
     * @return Incident
     */
    public function setImpactApplication($impactApplication)
    {
        $this->impactApplication = $impactApplication;

        return $this;
    }
    

    /**
     * Get dateFinImpact
     *
     * @return \DateTime
     */
    public function getDateFinImpact()
    {
        return $this->dateFinImpact;
    }





    /**
     * Set Client
     *
     * @param integer $idClient
     *
     * @return Incident
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get Client
     *
     * @return int
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set dureeResolution
     *
     * @param string $dureeResolution
     *
     * @return Incident
     */
    public function setDureeResolution($dureeResolution)
    {
        $this->dureeResolution = $dureeResolution;

        return $this;
    }

    /**
     * Get dureeResolution
     *
     * @return string
     */
    public function getDureeResolution()
    {
        return $this->dureeResolution;
    }

    /**
     * Add diffusion
     *
     * @param \AppBundle\Entity\Diffusion $diffusion
     *
     * @return Incident
     */
    public function addDiffusion(\AppBundle\Entity\Diffusion $diffusion)
    {
        $this->diffusions[] = $diffusion;

        return $this;
    }

    /**
     * Remove diffusion
     *
     * @param \AppBundle\Entity\Diffusion $diffusion
     */
    public function removeDiffusion(\AppBundle\Entity\Diffusion $diffusion)
    {
        $this->diffusions->removeElement($diffusion);
    }

    /**
     * Get diffusions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiffusions()
    {
        return $this->diffusions;
    }

    /**
     * Add po
     *
     * @param \AppBundle\Entity\Pos $po
     *
     * @return Incident
     */
    public function addPo(\AppBundle\Entity\Pos $po)
    {
        $this->pos[] = $po;

        return $this;
    }
    
    
     /**
     * Add sms
     *
     * @param \AppBundle\Entity\Sms $sms
     *
     * @return Incident
     */
    public function addSms(\AppBundle\Entity\Sms $sms)
    {
        $this->sms[] = $sms;

        return $this;
    }
    
    /**
     * Remove sms
     *
     * @param \AppBundle\Entity\Sms $sms
     */
    public function removeSms(\AppBundle\Entity\Sms $sms)
    {
        $this->sms->removeElement($sms);
    }
    

    /**
     * Remove po
     *
     * @param \AppBundle\Entity\Pos $po
     */
    public function removePo(\AppBundle\Entity\Pos $po)
    {
        $this->pos->removeElement($po);
    }

    /**
     * Get pos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPos()
    {
        return $this->pos;
    }
    
    /**
     * Get sms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSms()
    {
        return $this->sms;
    }

    
    
    
    /**
     * Add impactMetier
     *
     * @param \AppBundle\Entity\Metier $impactMetier
     *
     * @return Incident
     */
    public function addImpactMetier(\AppBundle\Entity\Metier $impactMetier)
    {
        $this->impactMetier[] = $impactMetier;

        return $this;
    }

    /**
     * Remove impactMetier
     *
     * @param \AppBundle\Entity\Metier $impactMetier
     */
    public function removeImpactMetier(\AppBundle\Entity\Metier $impactMetier)
    {
        $this->impactMetier->removeElement($impactMetier);
    }

    /**
     * Add impactApplication
     *
     * @param \AppBundle\Entity\Application $impactApplication
     *
     * @return Incident
     */
    public function addImpactApplication(\AppBundle\Entity\Application $impactApplication)
    {
        $this->impactApplication[] = $impactApplication;

        return $this;
    }

    /**
     * Remove impactApplication
     *
     * @param \AppBundle\Entity\Application $impactApplication
     */
    public function removeImpactApplication(\AppBundle\Entity\Application $impactApplication)
    {
        $this->impactApplication->removeElement($impactApplication);
    }
}
