<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Sms
 *
 * @ORM\Table(name="sms")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SmsRepository")
 */
class Sms
{
    
    
        public function __construct(){
        $this->dateEnvoi = new \DateTime() ;
    }
    
    
    /**
     * @Assert\IsTrue(message = "La taille du message ne doit pas dépasser 158 caractères")
     */
    public function isDateValid() // verifies que la taille du message est inférieure à 160 
    {
        return strlen($this->title."\n".$this->text) < 160;
    }
    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner le titre")
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    
    /**
    * Many SMS have One Incident.
    * @ORM\ManyToOne(targetEntity="Incident", inversedBy="sms")
    * @ORM\JoinColumn(name="id_incident", referencedColumnName="id")
    */
    private $incident;
    
    /**
     * Set idIncident
     *
     * @param integer $idIncident
     *
     * @return Sms
     */
    public function setIncident($incident)
    {
        $this->incident = $incident;

        return $this;
    }

    /**
     * Get idIncident
     *
     * @return Incident
     */
    public function getIncident()
    {
        return $this->incident;
    }
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnvoi", type="datetime")
     */
    private $dateEnvoi; // Date de l'envoie de l'sms

    
    public function setDateEnvoi($dateEnvoi)
    {
        $this->dateEnvoi = $dateEnvoi;

        return $this;
    }

    /**
     * Get dateEnvoi
     *@Assert\NotBlank(message = "La date d'envoie doit exister contactez un admin !")
     * @return \DateTime
     */
    public function getDateEnvoi()
    {
        return $this->dateEnvoi;
    }
    

    /**
     * @var string
     *@Assert\NotBlank(message="Veuillez renseigner le nom de le corps du sms")
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * Many sms have One sender.
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $emetteur;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Sms
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Sms
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set emetteur
     *
     * @param \stdClass $emetteur
     *
     * @return Sms
     */
    public function setEmetteur($emetteur)
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * Get emetteur
     *
     * @return \stdClass
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }
}

