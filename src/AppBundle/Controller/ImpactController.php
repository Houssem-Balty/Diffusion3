<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Impact;
use AppBundle\Entity\Client;
use Doctrine\ORM\Query\Expr;
use AppBundle\Form\MetierType;
use AppBundle\Form\ImpactType;

class ImpactController extends Controller
{
    /**
     * @Route("/impact/list", name="impact_list")
     */

  public function listAction(Request $request)
    {

        $qb = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Impact')
       ->createQueryBuilder('c');
			 
			 $qb->select(array('u'))
									->from('AppBundle:Impact', 'u')
									->where('u.importance = :importance')
									->setParameter('importance',0);
				
				$impacts = $qb->getQuery()->getResult();
				

									
    	 return $this->render("impact/impact.html.twig",array(
       'impacts' => $impacts,
      ));
    }
									


		 /**
     * @Route("/impact/details/{idImpact}", name="impact_details")
     */

  public function detailsAction(Request $request, $idImpact)
    {

      $impact = $this
			->getDoctrine()
			->getRepository('AppBundle:Impact')
			->findOneById($idImpact);

      

    	return $this->render("impact/impactDetails.html.twig",array(
        'impact' => $impact,
        'nomImpact' => $impact->getNomImpact(),
        'id' => $impact->getId(),
      ));
    }


    /**
     * @Route("/impact/add", name="impact_add")
     */

  public function addAction(Request $request)
    {
			
			 $qb = $this
        ->getDoctrine()
        ->getManager()
       ->getRepository('AppBundle:Impact')
       ->createQueryBuilder('c');
			 
			 $qb->select(array('u'))
									->from('AppBundle:Impact', 'u')
									->where('u.importance = :importance')
									->setParameter('importance',0);
				
				$impacts = $qb->getQuery()->getResult();
				
			
					// préparation du formulaire 
         $impact = new Impact();
         $form = $this->createForm(ImpactType::class,$impact);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()){
						$impact = $form->getData();
						$impact->setImportance(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($impact) ;
            $em->flush();
	          $this->addFlash('success','L\'impact est ajouté avec succès.');				
						return $this->redirectToRoute('impact_add');
						
         } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
             if (count($errors) > 0){
                foreach($errors as $error){
                  $this->addFlash('Erreur',$error->getMessage());
                }
             }

           }

         return $this->render("impact/impactAdd.html.twig",array(
           'form' => $form->createView(),
					 'impacts' => $impacts,
         ));

    }


        /**
         * @Route("/impact/update/{idImpact}", name="impact_update")
         */

      public function updateAction(Request $request, $idImpact)
        {
         $impact = $this->getDoctrine()
         ->getRepository("AppBundle:Impact")
         ->findOneById($idImpact) ;
         
         $form = $this->createForm(ImpactType::class,$impact);
         $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()){
						$impact = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success','L\'impact est mis à jour avec succès');
            return $this->redirectToRoute('impact_add');

         } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             if (count($errors) > 0){
               foreach($errors as $error){
                $this->addFlash('Erreur',$error->getMessage());
               }
             }             


           }

         return $this->render("impact/impactUpdate.html.twig",array(
           'form' => $form->createView(),
           'nomImpact' => $impact->getNomImpact(),
           'id' => $impact->getId(),
         ));
        
        }
        
         /**
         * @Route("/impact/delete/{idImpact}", name="impact_delete")
         */

      public function deleteAction(Request $request, $idImpact)
        {
            $em = $this->getDoctrine()->getManager();
            $impact = $this->getDoctrine()->getRepository("AppBundle:Impact")->findOneById($idImpact) ;
             $incident = $this->getDoctrine()->getRepository("AppBundle:Incident")
              ->createQueryBuilder('c')
              ->where("c.impact = :impact")
              ->setParameter('impact',$impact)
              ->getQuery()
              ->getResult();

         if ($impact == null){
          $this->addFlash('Erreur','L\'impact n\'existe pas.');
          return $this->redirect('/impact/add');
         }     

        if ($incident!=null){
          $this->addFlash('Erreur','Impact utilisé pour faire des diffusions ! Suppression impossible') ;
          return $this->redirect('/impact/add');
        } else if ($impact->getImportance() == 1 ){ 
          $this->addFlash('Erreur','Suppression impossible ! Impact Par défaut') ;
          return $this->redirect('/impact/add');
        }else {
            $em->remove($impact) ;
            $em->flush();
            $this->addFlash('success','L\'impact est supprimé avec succès.');
            return $this->redirect('/impact/add') ;
        }
           
        }
        
        /**
         * @Route("/metier/activate/{idImpact}", name="impact_activate")
         */

      public function activateAction(Request $request, $idImpact)
        {
           $impact = $this->getDoctrine()
           ->getRepository("AppBundle:Impact")
           ->findOneById($idImpact);
           
           if ($impact->getActivationStatus() == 0){
               $impact->setActivationStatus(1) ; 
           } else {
            $impact->setActivationStatus(0);
           }
           
           $em = $this->getDoctrine()
           ->getManager();
 
            $em->flush($impact);
            if ($impact->getActivationStatus()){
              $this->addFlash('success','L\'impact est activé avec succès') ;
            } else {
              $this->addFlash('success','L\'impact est désactivé avec succès') ;
            }
            
           return $this->redirect('/impact/add') ;
        }
}
