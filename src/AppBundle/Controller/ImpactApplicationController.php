<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use AppBundle\Entity\ImpactApplication;

class ImpactApplicationController extends Controller
{
    /**
     * @Route("/impactapplication/list", name="impactapplication_list")
     */

 public function listAction(Request $request)
    {

      $impactApplication = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:ImpactApplication')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

          return $this->render('impactApplication/impactApplication.html.twig', array('applications' => $impactApplication));

    }


    /**
     * @Route("/impactapplication/add", name="impactapplication_add")
     */

  public function addAction(Request $request){

    /*
    *   Définition d'une configuration Par défaut pour les Impactes Appli
    */
    $defaultImpactAppli = array() ;
    $lesAppli = $this->getDoctrine()
    ->getManager()
    ->getRepository('AppBundle:Application')
    ->createQueryBuilder('c')
    ->getQuery()->iterate();

    foreach ($lesAppli as $key => $value) {
      $defaultImpactAppli[$value[0]->getNomApplication()] = 0;
    }




    $ImpactApplication = new ImpactApplication();
    $ImpactApplication->setConfiguration($defaultImpactAppli);
    $ImpactApplicationFormBuilder = $this->createFormBuilder($ImpactApplication);
    $ImpactApplicationFormBuilder->add('nomImpactApplication',TextType::class);
    $ImpactApplicationFormBuilder->add('Configuration',CollectionType::class, array(
      'entry_type'   => ChoiceType::class,
      'entry_options'  => array(
   'choices'  => array(
     'Aucun' => 0,
     'Faible' => 1,
     'Moyen' => 2,
     'Fort'=> 3
    ),
   'multiple' => false,
   "expanded" => true ,
),
));


  $ImpactApplicationFormBuilder->add('submit',SubmitType::class,array('label' => 'Ajouter Configuration'));
  $ImpactApplicationForm = $ImpactApplicationFormBuilder->getForm();


  // Récupération des données depuis le formulaire
  $ImpactApplicationForm->handleRequest($request) ;
  if ($ImpactApplicationForm->isSubmitted() && $ImpactApplicationForm->isValid()){
       $ImpactApplicationConfig = $ImpactApplicationForm->getData();
       $em = $this->getDoctrine()->getManager();
       $em->persist($ImpactApplicationConfig) ;
       $em->flush();
}

  return $this->render('impactApplication/impactApplicationAdd.html.twig',
   array('addForm' =>$ImpactApplicationForm->createView() ));
  }

  /**
   * @Route("/impactapplication/update/{idConfig}", name="impactapplication_update")
   */

  public function updateAction($idConfig)
     {

       $ImpactApplication = $this->getDoctrine()
         ->getRepository('AppBundle:ImpactApplication')
         ->findOneById($idConfig);

         $ImpactApplicationFormBuilder = $this->createFormBuilder($ImpactApplication);
         $ImpactApplicationFormBuilder->add('nomImpactApplication',TextType::class);
         $ImpactApplicationFormBuilder->add('Configuration',CollectionType::class, array(
           'entry_type'   => ChoiceType::class,
           'entry_options'  => array(
        'choices'  => array(
          'Aucun' => 0,
          'Faible' => 1,
          'Moyen' => 2,
          'Fort'=> 3
         ),
        'multiple' => false,
        "expanded" => true ,
     ),
     ));


       $ImpactApplicationFormBuilder->add('submit',SubmitType::class,array('label' => 'Ajouter Configuration'));
       $ImpactApplicationForm = $ImpactApplicationFormBuilder->getForm();

       return $this->render('impactApplication/impactApplicationModify.html.twig',
        array('modifyForm' =>$ImpactApplicationForm->createView() ));

     }






}
