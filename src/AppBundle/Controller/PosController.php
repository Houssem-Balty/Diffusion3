<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\incident;
use AppBundle\Entity\Pos;
use AppBundle\Form\ApplicationType;
use AppBundle\Form\PosType;
class PosController extends Controller
{
    /**
     * @Route("/pos/list/{idIncident}", name="pos_list")
     */

 public function listAction(Request $request, $idIncident)
    {
        
      $poss = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Pos')
            ->createQueryBuilder('c')
            ->where("c.incident=".$idIncident)
            ->getQuery()->iterate();
            

          return $this->render('pos/posList.html.twig', array('poss' => $poss, 'incident' => $idIncident));

    }


    /**
     * @Route("/pos/add/{idIncident}", name="pos_add")
     */

 public function addAction(Request $request,$idIncident)
    {
      // Création d'un objet pos
       
       $pos= new Pos();  

       $incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident) ;

       $form = $this->createForm(PosType::class,$pos);
        $recentPos = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Pos')
            ->createQueryBuilder('c')
            ->where("c.incident=".$idIncident)
            ->getQuery()->iterate();

     
      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $data->setUserDeclare($this->getUser()) ;
             $data->setIncident($this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident)) ;
             if ($data->getIsResolution()){
               $data->setNextPosDate(null) ;
             } else{
               $data->setResolutionDate(null) ;
             }
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->persist($data);
             $em->flush();
             $this->addFlash('success','Point de situation ajouté avec succès !');
             return $this->redirect('/pos/add/'.$idIncident) ;
             
           }  else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             if (count($erros) > 0){
              foreach($errors as $error)
                $this->addFlash('Erreur',$error->getMessage());
             }

                   return $this->render('/pos/posAdd.html.twig',array(
                     'form' => $form->createView(),
                     'poss' => $recentPos,
                     'errors' => $errors,
                     'pageTitle' => 'Ajouter un point de situation'
               ));

           }
           
      return $this->render('/pos/posAdd.html.twig',array(
       'form' => $form->createView(),
       'poss' => $recentPos,
       'pageTitle' => 'Ajouter un point de situation',
       'incident' => $incident, 
       ));


    }
    
    
    
    /**
     * @Route("/pos/add/{idIncident}/{idPos}", name="pos_update")
     */

 public function updateAction(Request $request,$idIncident,$idPos)
    {
      // Création d'un objet Login
       
       $pos= $this->getDoctrine()->getRepository('AppBundle:Pos')->findOneById($idPos);

       $incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident) ;

       $form = $this->createForm(PosType::class,$pos);
       
        $recentPos = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Pos')
            ->createQueryBuilder('c')
            ->where("c.incident=".$idIncident)
            ->getQuery()->iterate();

     
      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $data->setUserDeclare($this->getUser()) ;
             $data->setIncident($this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident)) ;
              if ($data->getIsResolution()){
               $data->setNextPosDate(null) ;
             } else{
               $data->setResolutionDate(null) ;
             }
             $em = $this->getDoctrine()->getEntityManager() ;
             $em->flush();
             $this->addFlash('success','Point de situation mis à jour avec succès !');
             return $this->redirectToRoute('pos_add',array('idIncident' => $idIncident));
             
           }
           
      return $this->render('/pos/posAdd.html.twig',array(
       'form' => $form->createView(),
       'poss' => $recentPos,
       'pageTitle' => 'Modifier un point de situation',
       'incident' => $incident,
       ));


    }
    
    
    
    
    
     /**
     * @Route("/pos/delete/{idIncident}/{idPos}", name="pos_delete")
     */

 public function deleteAction(Request $request,$idIncident, $idPos)
    {
        $pos = $this->getDoctrine()
        ->getRepository('AppBundle:Pos')
        ->findOneById($idPos) ;

        if ($pos == null){
          $this->addFlash('Erreur','Le pos n\'existe pas.'); 
          return $this->redirectToRoute("pos_add",array('idIncident' =>$idIncident));
        }
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($pos);
        $em->flush();
        
        return $this->redirectToRoute("pos_add",array('idIncident' =>$idIncident));
    }



}
